const { Component } = require("react");

class Steps extends Component {
    render () {
        let {id, title, content} = this.props;
        return (
            <p>
                {id}. {title}: {content}
            </p>
        )
    }
}

export default Steps;